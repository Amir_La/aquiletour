import styled from 'styled-components';
import { useEffect, useState } from 'react';
import XLSX from 'xlsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDownload } from '@fortawesome/free-solid-svg-icons'
import Logo from './logo.png';
const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  color: #fff;
  overflow: hidden;
`;
const StyledCard = styled.div`
  width: 70%;
  margin: 0 auto;
  padding: 5px;
  @media screen and (max-width:500px)
  {
    width: 100%;
  }
  @media screen and (max-width: 450px)
  {
    padding: 0px;
  }
`;
const TitleContainer = styled.div`
  padding: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const ImageTitle = styled.img`
  width: 100px;
  height: 100px;
`;
const Title = styled.h1`
  font-size: 30px;
  text-align: center;
  margin: 2px;
`;
const InfoContainer = styled.div`

`;
const Info = styled.p`
  color: #adb5bd;
  padding: 0px;
  text-align: center;
`;
const InputContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  padding: 5px;
  @media screen and (max-width: 450px)
  {
    padding: 0px;
  }
`;
const ExcelInput = styled.input`
  width: 0.1px;
  height: 0.1px;
  opacity: 0;
  overflow: hidden;
  position: absolute;
  z-index: -1;
`;
const ChooseButtonContainer = styled.div`
  display: flex;
  justify-content: space-around;
  padding: 10px 1px;
  flex-wrap: wrap;
`;
const ChooseButton = styled.button`
  font-size: 1.25em;
  font-weight: 700;
  color: #FFFFFF;
  background-color: #5781ff;
  padding: 5px 15px;
  border-radius: 5px;
  cursor: pointer;
  border: 0;
  margin: 5px;
`;
const TableCard = styled.td`
  margin: 5px;
  border-bottom: 1px solid #ced4da;
  padding: 5px;
  text-align: center;
`;

const TableContainer = styled.table`
  color: #fff;
  border: 1px solid  #ced4da;
  border-radius: 5px;
  margin: 15px auto;
  border-spacing: 0px;
  overflow: hidden;
  @media screen and (max-width:600px)
  {
    font-size: 11px;
  }
  @media screen and (max-width: 350px)
  {
    font-size: 8px;
  }
`;
const ListCard =styled.div`
  padding: 15px;
`; 
const CardListContainer = styled.div`
  color: #fff;
  border: 1px solid  #ced4da;
  border-radius: 5px;
  margin: 15px 5px;
  border-spacing: 0px;
  overflow: hidden;
  padding: 0px;
  @media screen and (max-width:500px)
  {
    font-size: 11px;
  }
  @media screen and (max-width: 350px)
  {
    font-size: 8px;
  }
`;
const ListRow = styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: scroll;
  &::-webkit-scrollbar {
    width: 12px;
  }
  
  &::-webkit-scrollbar-track {
   background-color: #171721;
  }
  
  &::-webkit-scrollbar-thumb {
    background-color: #ced4da;
    border-radius: 20px;
    border: 2px solid #171721;
  }
  scrollbar-width: thin;
  height: 150px;
`;
const ListHeaderRow = styled.div`
  display: flex;
  flex-direction: column;
`;
const ListHeader = styled.div`
  background:  #FFD557;
  color: #171721;
  text-align: center;
  font-weight: bold;
  padding: 5px;
`;
const ListsContainer = styled.div`
  display: flex;
  justify-content: center;
`;
const TableRowMeetUp = styled.tr`
  padding: 5px;
`;
const TableHeader = styled.th`
  background:  #FFD557;
  color: #171721;
  padding: 5px;
`;
const TableCaption = styled.caption`
  color: #FFFFFF;
`;
const ExcelLabel = styled.label`
  display: flex;
  justify-content: space-around;
  font-size: 1.25em;
  font-weight: 700;
  color: #FFFFFF;
  background-color: #5781ff;
  padding: 5px 15px;
  border-radius: 5px;
  cursor: pointer;
  display: inline-block;
  #file:focus + & {
    outline: 1px dotted #000;
    outline: -webkit-focus-ring-color auto 5px
  }
`;
const SwitchContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
const SwitchMode = styled.div`
  border: 1px solid #fff;
  border-radius: 5px;
  display: flex;
`;
const SwitchButton = styled.div`
  color: ${props => props.active ? '#171721' : '#FFF'};
  background-color: ${props => props.active ? '#FFD557' : '#171721'};
  font-weight: ${props => props.active ? '700' : '200'};
  cursor: pointer;
  border-radius: 5px;
  margin: 5px;
  padding: 5px;
`;


const RoleContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
const RoleCard = styled.div`
  padding: 15px;
  margin: 10px 0px;
`;
const TitleCard = styled.div`
  padding: 5px;
  font-weight: 700;
`;
const Participant = styled.div`
  text-align: center;
`;
const DateContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;
  margin-left: 15px;
  @media screen and (max-width:500px)
  {
    margin-top: 8px;
  }
`;
const LabelDate = styled.label`

`;
const InputDate = styled.input`
  padding: 5px;
  font-weight: 700;
  font-family: inherit;
  border: 0;
  border-radius: 5px;
  border: 2px solid #171721;
  height: 28px;
  &:hover {
    border: 2px solid #5781ff;
  }
`;

const OptionsContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-end;
  padding: 5px 0px;
  @media screen and (max-width:500px)
  {
    flex-direction: column;
    align-items: center;
  }
`;


function App() {
  const [data,setData] = useState(null);
  const [filename,setFilename] = useState("Choisir un fichier")
  const [roles,setRoles] = useState([]);
  const [participants,setParticipants] = useState([]);
  const [orderParticipants,setOrderParticipants] = useState([]);
  const [roleNextMeetUp,setRoleNextMeetUp] = useState([]);
  const [lastRoleMeetUp,setLastRoleMeetUp] = useState([]);
  const [lastMeetUp,setLastMeetUp] = useState([]);
  const [nextMeetUp,setNextMeetUp] = useState([]);
  const [filledMeetUp,setFilledMeetUp] = useState(false);
  const [forEachMode,setForEachMode] = useState(true);
  const thisDate = new Date();
  const [date,setDate] = useState(`${thisDate.getDate()}/${thisDate.getMonth()+1}/${thisDate.getFullYear()}`);

  const normalizeDate = (data) => {
    let newData = []
    for (let index = 0; index < data.length; index++) {
      var dataRefurbished = [...data[index]];
      if(index > 1 && typeof data[index][0] == "number")
      {
        var utc_days  = Math.floor(data[index][0] - 25569);
        var utc_value = utc_days * 86400; 
        var date_info = new Date(utc_value*1000);
        dataRefurbished.splice(0,1,`${date_info.getDate()}/${date_info.getMonth()+1}/${date_info.getFullYear()}`);
      }
      newData.push(dataRefurbished);
    }
    return newData;
  }

  const handleChange = (evt) => {
    const file = evt.target.files[0];
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;
    if(file.name)
    {
      setFilename(file.name);
    }
    reader.onload = e => {
      /* Parse data */
      const bstr = e.target.result;
      const wb = XLSX.read(bstr, { type: rABS ? "binary" : "array" });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      let excelData = XLSX.utils.sheet_to_json(ws, { header: 1 });
      /* Update state */
      let newData = [...excelData];
      newData = normalizeDate(newData);
      if(excelData[0][0] == "Roles"){
        let role = [...excelData[0]];
        role.shift();
        setRoles(role);
      }
      if(excelData[1][0] == "Date")
      {
        let participant = [...excelData[1]];
        participant.shift();
        const newParticipants = participant.map((p,id) => {return {name: p,id}});
        setParticipants(newParticipants);
        if(excelData.length > 2)
        { 

          let newLastMeetUp = [...newData[newData.length-1]];
          setLastMeetUp(newLastMeetUp);
          let lastDate = newLastMeetUp[0];
          newLastMeetUp.shift();
          let lastRoleMeetUp = newLastMeetUp.map((r,id) => {return {role: r,name: participant[id],id}}).filter(r => r.role !== "-");
          lastRoleMeetUp.splice(0,0,lastDate);
          setLastRoleMeetUp(lastRoleMeetUp);
        }

      }
      setData(newData);
    };
    if (rABS) reader.readAsBinaryString(file);
    else reader.readAsArrayBuffer(file);

  }
  const exportFile = () => {
    /* convert state to workbook */
    let newNextMeetUp = [...nextMeetUp];
    let newDate = new Date(date);
    newNextMeetUp.splice(0,1,`${newDate.getDate()}/${newDate.getMonth()+1}/${newDate.getFullYear()}`);
    const ws = XLSX.utils.aoa_to_sheet([...data,newNextMeetUp]);
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "SheetJS");
    /* generate XLSX file and send to client */
    XLSX.writeFile(wb, `reunion${thisDate.getDate()}/${thisDate.getMonth()+1}/${thisDate.getFullYear()}.xlsx`);
  }

  useEffect(() => { fillArray();},[participants])
  
  const fillArray = () => {
    let newArray = [];
    let newParticipants = [];

    newArray.push(`${thisDate.getDate()}/${thisDate.getMonth()+1}/${thisDate.getFullYear()}`);

    for(let i = 0; i < participants.length; i++) {
      newArray.push("-");
    }
    for(let j = 0; j < roles.length; j++) {
      newParticipants.push("-");
    }
    setNextMeetUp(newArray);
    setOrderParticipants(newParticipants);
  }
  const reset = () => {
    fillArray();
    setFilledMeetUp(false);
  }
  const chooseParticipant = () => {
    let availableRole = [...roles];
    let availableParticipant = [...participants];
    let maxRole = availableRole.length;
    let maxParticipant = availableParticipant.length;
    let meetUp = [...nextMeetUp];
    let roledMeetUp = [];
    let chosenParticipants = [];
    let lastParticipantId = 0;
    let lastRoleId = 0;

    while(maxRole <= maxParticipant ? maxRole > 0 : maxParticipant > 0){
      let idParticipant = Math.floor(Math.random() * maxParticipant);
      let idRole = Math.floor(Math.random() * maxRole);
      let chosenParticipant = availableParticipant[idParticipant];
      let chosenRole = availableRole[idRole];
      //Test if last role of the participant before accept 
      if(lastMeetUp[chosenParticipant.id + 1] !== chosenRole && availableParticipant.length >= 1) {
        chosenParticipants.push({p: chosenParticipant,role: chosenRole});
        availableRole.splice(idRole, 1);
        availableParticipant.splice(idParticipant, 1);
        maxRole--;
        maxParticipant--;
      }
      // error case go backward
      else if (lastMeetUp[chosenParticipant.id + 1] == chosenRole && availableParticipant.length == 1)
      {
        availableRole.splice(lastRoleId,0,chosenParticipants[lastParticipantId + 1].role);
        availableParticipant.splice(lastParticipantId,0,chosenParticipants[lastParticipantId + 1].p);
        maxRole++;
        maxParticipant++;
      }

    }
    chosenParticipants.forEach(element => {
      meetUp.splice(element.p.id +1,1,element.role);
    });

    setNextMeetUp(meetUp);
    setRoleNextMeetUp(chosenParticipants);
    setFilledMeetUp(true);
  }

  const chooseParticipantByRole = (role,index) => {
    let oldRoleIdMeetUp = nextMeetUp.findIndex(oldRole => oldRole == role);
    let newMeetUp = [...nextMeetUp];
    let newOrderParticipants = [...orderParticipants];
    let isChoose = false;
    while(!isChoose)
    {
      let idParticipant = Math.floor(Math.random() * participants.length);
      let participant = participants[idParticipant];
      if(oldRoleIdMeetUp > 0)
      {
        newMeetUp.splice(oldRoleIdMeetUp, 1,"-");
      }
      if(lastMeetUp[participant.id] !== role && newOrderParticipants.findIndex(p => p == participant.name) < 0)
      {
        newMeetUp.splice(idParticipant+1,1,role);
        newOrderParticipants.splice(index,1,participant.name);
        isChoose = true;
        setNextMeetUp(newMeetUp);
        setOrderParticipants(newOrderParticipants);
      }
    }

  }

  const handleSwitchMode = () => {
    fillArray();
    setForEachMode(!forEachMode);
  }
  const handleDate = (value) => {
    setDate(value);
  }

  return (
    <Container>
      <StyledCard>
        <TitleContainer>
          <ImageTitle src={Logo} />
          <Title> A qui le tour ? </Title>
        </TitleContainer>
        <InfoContainer>
          <Info>
            Pour commencer veuillez charger un fichier excel déjà formaté
          </Info>
        </InfoContainer>
        <InputContainer>
          <ExcelInput id="file" name="file" type="file" onChange={(evt) => handleChange(evt)} />
          <ExcelLabel for="file"> <FontAwesomeIcon icon={faDownload} /> {filename}</ExcelLabel>
        </InputContainer>
        <ListsContainer>
          {
            roles.length > 0 ? 
            <CardListContainer>
              <ListHeaderRow>
                <ListHeader scope="col"> Rôles </ListHeader>
              </ListHeaderRow>
              <ListRow>
                  {
                    roles.map(role => <ListCard> {role} </ListCard>)
                  }  
              </ListRow>
            </CardListContainer>
            : ""
          }
          {
            participants.length > 0 ? 
          <CardListContainer>
            <ListHeaderRow>
              <ListHeader scope="col"> Participants </ListHeader>
            </ListHeaderRow>
            <ListRow>
              {
                participants.map(part => <ListCard> { part.name } </ListCard>)
              }  
            </ListRow>
          </CardListContainer>
            : ""
          }
        </ListsContainer>
        {
          lastMeetUp.length > 0 ?
          <TableContainer>
            <TableCaption>Dernière réunion en date</TableCaption>
            <TableRowMeetUp>
              <TableHeader scope="col"> Date </TableHeader>
              {
                roles.map(role => <TableHeader scope="col">{role}</TableHeader>)
              }
            </TableRowMeetUp>
            <TableRowMeetUp>
              {
                //lastRoleMeetUp.map(mt => <TableCard> {mt.role ? mt.role : mt} </TableCard>)
                lastRoleMeetUp.map((part,id) => id == 0 ? <TableCard>{part}</TableCard> : lastRoleMeetUp.find(p => p.name && roles[id-1] == p.role) ? <TableCard> {lastRoleMeetUp.find(p => p.name && roles[id-1] == p.role).name} </TableCard> : null )
              }
            </TableRowMeetUp>
          </TableContainer> : ""
        }
        
        {
          participants.length > 0 ? 
          <OptionsContainer>
            <SwitchContainer>
              <SwitchMode>
                <SwitchButton active={forEachMode} onClick={() => handleSwitchMode()}> Par Participant </SwitchButton>
                <SwitchButton active={!forEachMode} onClick={() => handleSwitchMode()}> Aléatoire </SwitchButton>
              </SwitchMode>
            </SwitchContainer>
            <DateContainer>
              <LabelDate for="date"> Date de la prochaine réunion </LabelDate>
              <InputDate type="date" name="date" value={date} onChange={(e) => handleDate(e.target.value)}/>
            </DateContainer>
          </OptionsContainer>
           : null   
        }
        
        {
          forEachMode ? 
          <RoleContainer>
            {roles.map((role,id) => <RoleCard> <TitleCard>{role}</TitleCard> <ChooseButton onClick={() => chooseParticipantByRole(role,id)}> Lancer </ChooseButton> <Participant> {orderParticipants[id]} </Participant> </RoleCard>)}
          </RoleContainer>
          :
          filledMeetUp ?
          <TableContainer>
            <TableCaption>Prochaine Réunion</TableCaption>
            <TableRowMeetUp>
              {
                roleNextMeetUp.map(part => <TableHeader scope="col"> { part.role } </TableHeader>)
              }
            </TableRowMeetUp>
            <TableRowMeetUp>
            { 
              [...roleNextMeetUp.map(mt => <TableCard> {mt.p.name} </TableCard>)]
            }</TableRowMeetUp>
          </TableContainer> : 
          roles.length > 0 ? 
          <Info>Lancer le tirage pour savoir quelle sera la prochaine line up</Info>
          : null
        }
        <ChooseButtonContainer>
          {
            roles.length > 0 && !filledMeetUp && !forEachMode ? 
              <ChooseButton onClick={() => chooseParticipant()} > A qui le tour ? </ChooseButton> : filledMeetUp ? 
                <ChooseButton onClick={() => reset()} > Recommencer le tirage </ChooseButton> 
                : null
              }
          {
            participants.length > 0 && forEachMode || filledMeetUp ?
              <ChooseButton onClick={() => exportFile()} > Exporter le fichier </ChooseButton> : 
            ""
          }
        </ChooseButtonContainer>
        
      </StyledCard>
    </Container>
  );
}

export default App;
